package com.iomis.rest.dto.football;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class LineupsDto {
    @JsonProperty("home_team")
    private TeamDataDto homeTeam;
    @JsonProperty("away_team")
    private TeamDataDto awayTeam;

    @Data
    public static class TeamDataDto {
        @JsonProperty("starting_lineups")
        private List<FootballPlayerDto> startingLineups;
        private List<CoachDto> coaches;
    }
}

