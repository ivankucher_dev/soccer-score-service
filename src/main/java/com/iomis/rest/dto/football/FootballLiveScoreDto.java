package com.iomis.rest.dto.football;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.LocalDate;
import java.util.List;

@Data
public class FootballLiveScoreDto {
    @JsonProperty("result")
    private List<FootballLiveScoreResultDto> footballLiveScoreResults;

    @Data
    public static class FootballLiveScoreResultDto {
        @JsonProperty("event_key")
        private int eventKey;
        @JsonProperty("event_date")
        private LocalDate eventDate;
        @JsonProperty("event_time")
        private String eventTime;
        @JsonProperty("event_home_team")
        private String eventHomeTeam;
        @JsonProperty("event_away_team")
        private String eventAwayTeam;
        @JsonProperty("event_final_result")
        private String evenFinalResult;
        @JsonProperty("country_name")
        private String countryName;
        @JsonProperty("league_name")
        private String leagueName;
        @JsonProperty("league_key")
        private int leagueKey;
        @JsonProperty("league_round")
        private String leagueRound;
        @JsonProperty("league_season")
        private String leagueSeason;
        @JsonProperty("event_stadium")
        private String eventStadium;
        @JsonProperty("goalscorers")
        private List<GoalScorersDto> goalScorers;
        private List<CardDto> cards;
        private LineupsDto lineups;

        @Data
        public static class GoalScorersDto {
            private String time;
            private String score;
            @JsonProperty("away_scorer")
            private String awayScorer;
        }

        @Data
        public static class CardDto {
            private String time;
            private String card;
            @JsonProperty("away_fault")
            private String awayFault;
        }
    }

}
