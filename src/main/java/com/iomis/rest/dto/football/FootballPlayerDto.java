package com.iomis.rest.dto.football;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class FootballPlayerDto {
    @JsonProperty("player_number")
    private int playerNumber;
    private String player;
    @JsonProperty("player_country")
    private String playerCountry;
}
