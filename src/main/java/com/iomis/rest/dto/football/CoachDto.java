package com.iomis.rest.dto.football;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class CoachDto {
    private String coache;
    @JsonProperty("coache_country")
    private String coacheCountry;
}
