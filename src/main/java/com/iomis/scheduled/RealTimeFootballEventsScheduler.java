package com.iomis.scheduled;

import com.iomis.domain.entity.football.Coach;
import com.iomis.domain.entity.football.FootballPlayer;
import com.iomis.domain.entity.football.event.FootballEvent;
import com.iomis.domain.entity.football.event.FootballTeam;
import com.iomis.domain.entity.football.event.Lineup;
import com.iomis.rest.dto.football.FootballLiveScoreDto;
import com.iomis.rest.dto.football.FootballLiveScoreDto.FootballLiveScoreResultDto;
import com.iomis.rest.dto.football.LineupsDto;
import com.iomis.rest.dto.football.LineupsDto.TeamDataDto;
import com.iomis.service.CoachService;
import com.iomis.service.FootballEventService;
import com.iomis.service.FootballLiveScoreService;
import com.iomis.service.FootballPlayerService;
import com.iomis.service.mapper.FootballEventMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class RealTimeFootballEventsScheduler {

    private final FootballLiveScoreService footballLiveScoreService;
    private final FootballEventService footballEventService;
    private final CoachService coachService;
    private final FootballPlayerService footballPlayerService;
    private final FootballEventMapper footballEventMapper;

    //@Scheduled(cron = "${application.scheduled.download-real-time-football-data-and-produce-to-kafka-scheduled-job}")
    @Scheduled(initialDelay = 0, fixedDelay = Long.MAX_VALUE)
    public void downloadRealTimeFootballDataSaveAndProduceToKafkaScheduledJob() {
        Optional.ofNullable(footballLiveScoreService.getFootballLiveScoreDataFromApi())
                .map(FootballLiveScoreDto::getFootballLiveScoreResults)
                .ifPresentOrElse(this::processRealTimeFootballDataAndProduceToKafka,
                        () -> log.error("Cannot process football real time data in case of null from response of AllSportsApi."));
    }

    private void processRealTimeFootballDataAndProduceToKafka(List<FootballLiveScoreResultDto> footballLiveScoreResults) {
        footballLiveScoreResults.stream()
                .filter(Objects::nonNull)
                .map(footballLiveScoreResultDto ->
                        initLineupForFootballEventByFootballLiveScoreResultsDto(
                                footballLiveScoreResultDto,
                                footballEventMapper.mapFootballLiveScoreResultDtoToFootballEvent(footballLiveScoreResultDto)))
                .map(footballEventService::saveFootballEventIfNotExist)
                .forEach(footballEvent -> System.out.println("Processing event with id " + footballEvent.getId() + " and event key " + footballEvent.getEventKey() + " and date " + footballEvent.getEventDate() + " to kafka"));
    }

    private FootballEvent initLineupForFootballEventByFootballLiveScoreResultsDto(FootballLiveScoreResultDto footballLiveScoreResultsDto, FootballEvent footballEvent) {
        log.info("Initiating lineup for event with key {} and for date {}", footballLiveScoreResultsDto.getEventKey(), footballLiveScoreResultsDto.getEventDate());
        footballEvent.setLineups(new Lineup(
                Optional.ofNullable(footballLiveScoreResultsDto.getLineups())
                        .map(LineupsDto::getHomeTeam)
                        .map(this::initFootballTeamByCoachesAndPlayers)
                        .orElse(null),
                Optional.ofNullable(footballLiveScoreResultsDto.getLineups())
                        .map(LineupsDto::getAwayTeam)
                        .map(this::initFootballTeamByCoachesAndPlayers)
                        .orElse(null)));

        log.info("Lineup {} initiated for event with key {} and for date {}", footballEvent.getLineups(), footballLiveScoreResultsDto.getEventKey(), footballLiveScoreResultsDto.getEventDate());

        return footballEvent;
    }

    private FootballTeam initFootballTeamByCoachesAndPlayers(TeamDataDto teamDataDto) {
        return new FootballTeam(
                teamDataDto.getStartingLineups().stream()
                        .map(footballPlayerService::findOrSaveFootballPlayerIfNotExists)
                        .map(FootballPlayer::getId)
                        .collect(Collectors.toList()),
                teamDataDto.getCoaches().stream()
                        .map(coachService::findOrSaveCoachIfNotExists)
                        .map(Coach::getId)
                        .collect(Collectors.toList()));
    }
}
