package com.iomis.domain.repository;

import com.iomis.domain.entity.football.event.FootballEvent;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface FootballEventRepository extends MongoRepository<FootballEvent, String> {
    Optional<FootballEvent> findByEventKeyAndEventDate(int eventKey, LocalDate eventDate);
}
