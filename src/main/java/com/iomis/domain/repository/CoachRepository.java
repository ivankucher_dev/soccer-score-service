package com.iomis.domain.repository;

import com.iomis.domain.entity.football.Coach;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CoachRepository extends MongoRepository<Coach, String> {
    Optional<Coach> findOneByCoacheAndCoacheCountry(String coache, String coacheCountry);
}
