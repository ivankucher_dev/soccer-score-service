package com.iomis.domain.repository;

import com.iomis.domain.entity.football.FootballPlayer;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface FootballPlayerRepository extends MongoRepository<FootballPlayer, String> {
    Optional<FootballPlayer> findByPlayerFullNameAndPlayerCountry(String playerFullName, String playerCountry);
}
