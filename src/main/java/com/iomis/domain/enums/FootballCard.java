package com.iomis.domain.enums;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.Arrays;

@Getter
@RequiredArgsConstructor
public enum FootballCard {
    YELLOW("yellow card"),
    RED("red card");

    private final String value;

    public static FootballCard forValue(String value) {
        return Arrays.stream(FootballCard.values())
                .filter(footballCard -> footballCard.getValue().equals(value))
                .findFirst()
                .orElse(null);
    }
}
