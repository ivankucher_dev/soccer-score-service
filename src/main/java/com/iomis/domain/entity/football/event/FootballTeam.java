package com.iomis.domain.entity.football.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FootballTeam {
    @Field("team_players")
    private List<String> teamPlayers;
    @Field("team_coaches")
    private List<String> coaches;
}
