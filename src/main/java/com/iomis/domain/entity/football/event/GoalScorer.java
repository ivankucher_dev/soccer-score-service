package com.iomis.domain.entity.football.event;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
public class GoalScorer {
    private String time;
    private String score;
    @Field("away_scorer")
    private String awayScorer;
}
