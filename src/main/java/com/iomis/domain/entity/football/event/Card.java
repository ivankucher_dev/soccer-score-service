package com.iomis.domain.entity.football.event;

import com.iomis.domain.enums.FootballCard;
import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
public class Card {
    private String time;
    private FootballCard card;
    @Field("away_fault")
    private String awayFault;
}
