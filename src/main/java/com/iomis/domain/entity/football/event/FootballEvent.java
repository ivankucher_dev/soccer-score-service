package com.iomis.domain.entity.football.event;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDate;
import java.util.List;

@Data
@Document("football_event")
public class FootballEvent {

    @Id
    private String id;
    @Field("event_key")
    private int eventKey;
    @Field("event_date")
    private LocalDate eventDate;
    @Field("event_time")
    private String eventTime;
    @Field("event_home_team")
    private String eventHomeTeam;
    @Field("event_away_team")
    private String eventAwayTeam;
    @Field("event_final_result")
    private String evenFinalResult;
    @Field("country_name")
    private String countryName;
    @Field("league_name")
    private String leagueName;
    @Field("league_key")
    private int leagueKey;
    @Field("league_round")
    private String leagueRound;
    @Field("league_season")
    private String leagueSeason;
    @Field("event_stadium")
    private String eventStadium;
    @Field("goal_scorers")
    private List<GoalScorer> goalScorers;
    private List<Card> cards;
    private Lineup lineups;
}
