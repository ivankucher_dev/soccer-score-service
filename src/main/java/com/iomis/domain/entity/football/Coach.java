package com.iomis.domain.entity.football;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document("football_coach")
public class Coach {
    @Id
    private String id;
    private String coache;
    @JsonProperty("coache_country")
    private String coacheCountry;
}
