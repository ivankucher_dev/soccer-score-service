package com.iomis.domain.entity.football.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Lineup {
    @Field("home_team")
    private FootballTeam homeTeam;
    @Field("away_team")
    private FootballTeam awayTeam;
}
