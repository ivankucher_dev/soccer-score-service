package com.iomis.domain.entity.football;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

@Data
@Document("football_player")
public class FootballPlayer {
    @Id
    private String id;
    @Field("player_number")
    private int playerNumber;
    @Field("player_full_name")
    private String playerFullName;
    @Field("player_country")
    private String playerCountry;
}
