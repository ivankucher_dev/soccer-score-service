package com.iomis.service.kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class KafkaConsumer {

    private static final String TOPIC = "test-topic";

    @KafkaListener(topics = TOPIC)
    public void consume(String message) {
        log.info("Consumed message -> {}", message);
    }
}