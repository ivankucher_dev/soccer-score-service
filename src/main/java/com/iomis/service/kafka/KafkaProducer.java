package com.iomis.service.kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class KafkaProducer {

    private static final String TOPIC = "test-topic";

    private final KafkaTemplate<String, String> kafkaTemplate;

    @PostConstruct
    public void test() {
        sendMessage("Initial message");
    }

    public void sendMessage(String message) {
        log.info("Produce message {} to topic {}", message, TOPIC);
        this.kafkaTemplate.send(TOPIC, message);
    }
}