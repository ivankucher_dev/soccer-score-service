package com.iomis.service;

import com.iomis.domain.entity.football.FootballPlayer;
import com.iomis.domain.repository.FootballPlayerRepository;
import com.iomis.rest.dto.football.FootballPlayerDto;
import com.iomis.service.mapper.FootballPlayerMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FootballPlayerService {
    private final FootballPlayerRepository footballPlayerRepository;
    private final FootballPlayerMapper footballPlayerMapper;

    public FootballPlayer findOrSaveFootballPlayerIfNotExists(FootballPlayerDto footballPlayerDto) {
        return footballPlayerRepository.findByPlayerFullNameAndPlayerCountry(footballPlayerDto.getPlayer(), footballPlayerDto.getPlayerCountry())
                .orElse(footballPlayerRepository.save(footballPlayerMapper.mapFootballPlayerDtoToEntity(footballPlayerDto)));
    }
}
