package com.iomis.service.mapper;

import com.iomis.domain.entity.football.Coach;
import com.iomis.rest.dto.football.CoachDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface CoachMapper {
    @Mapping(target = "id", ignore = true)
    Coach mapCoachDtoToCoachEntity(CoachDto coachDto);
}
