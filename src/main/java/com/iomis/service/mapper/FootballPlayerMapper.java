package com.iomis.service.mapper;

import com.iomis.domain.entity.football.FootballPlayer;
import com.iomis.rest.dto.football.FootballPlayerDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface FootballPlayerMapper {
    @Mapping(target = "playerFullName", source = "player")
    @Mapping(target = "id", ignore = true)
    FootballPlayer mapFootballPlayerDtoToEntity(FootballPlayerDto footballPlayerDto);

    @Mapping(target = "player", source = "playerFullName")
    FootballPlayerDto mapFootballPlayerEntityToDto(FootballPlayer footballPlayer);
}
