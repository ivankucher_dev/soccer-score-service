package com.iomis.service.mapper;

import com.iomis.domain.entity.football.event.Card;
import com.iomis.domain.entity.football.event.FootballEvent;
import com.iomis.domain.entity.football.event.GoalScorer;
import com.iomis.domain.enums.FootballCard;
import com.iomis.rest.dto.football.FootballLiveScoreDto.FootballLiveScoreResultDto;
import com.iomis.rest.dto.football.FootballLiveScoreDto.FootballLiveScoreResultDto.CardDto;
import com.iomis.rest.dto.football.FootballLiveScoreDto.FootballLiveScoreResultDto.GoalScorersDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface FootballEventMapper {

    @Mapping(target = "lineups", ignore = true)
    FootballEvent mapFootballLiveScoreResultDtoToFootballEvent(FootballLiveScoreResultDto footballLiveScoreResultDto);

    GoalScorer mapGoalScorerDtoToGoalScorer(GoalScorersDto goalScorersDto);

    Card cardDtoToCardEntity(CardDto cardDto);

    default FootballCard valueToFootballCard(String value) {
        return FootballCard.forValue(value);
    }
}
