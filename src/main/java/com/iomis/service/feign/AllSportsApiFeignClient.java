package com.iomis.service.feign;

import com.iomis.rest.dto.football.FootballLiveScoreDto;
import com.iomis.service.feign.fallback.AllSportsApiFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "allSportsApi", url = "${application.all_sports_api.api_host}", fallbackFactory = AllSportsApiFallbackFactory.class)
public interface AllSportsApiFeignClient {
    @RequestMapping(method = RequestMethod.GET, value = "/football")
    FootballLiveScoreDto findTodayFootballMatchesData(@RequestParam(value = "met") String timeType, @RequestParam(value = "APIkey") String apiKey);
}
