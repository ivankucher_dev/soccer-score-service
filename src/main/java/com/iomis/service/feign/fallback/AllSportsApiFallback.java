package com.iomis.service.feign.fallback;

import com.iomis.rest.dto.football.FootballLiveScoreDto;
import com.iomis.service.feign.AllSportsApiFeignClient;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
public class AllSportsApiFallback implements AllSportsApiFeignClient {

    private final Throwable cause;

    @Override
    public FootballLiveScoreDto findTodayFootballMatchesData(String timeType, String apiKey) {
        log.error("[All sports] API is not responding for timeType {}. Reason: {}", timeType, cause.getMessage(), cause);
        return null;
    }
}
