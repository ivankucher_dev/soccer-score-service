package com.iomis.service.feign.fallback;

import com.iomis.service.feign.AllSportsApiFeignClient;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class AllSportsApiFallbackFactory implements FallbackFactory<AllSportsApiFeignClient> {

    @Override
    public AllSportsApiFeignClient create(Throwable cause) {
        return new AllSportsApiFallback(cause);
    }

}