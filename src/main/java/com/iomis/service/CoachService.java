package com.iomis.service;

import com.iomis.domain.entity.football.Coach;
import com.iomis.domain.repository.CoachRepository;
import com.iomis.rest.dto.football.CoachDto;
import com.iomis.service.mapper.CoachMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class CoachService {

    private final CoachRepository coachRepository;
    private final CoachMapper coachMapper;

    public Coach findOrSaveCoachIfNotExists(CoachDto coachDto) {
        return coachRepository.findOneByCoacheAndCoacheCountry(coachDto.getCoache(), coachDto.getCoacheCountry())
                .orElse(coachRepository.save(coachMapper.mapCoachDtoToCoachEntity(coachDto)));
    }
}
