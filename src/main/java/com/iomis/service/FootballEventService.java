package com.iomis.service;

import com.iomis.domain.entity.football.event.FootballEvent;
import com.iomis.domain.repository.FootballEventRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FootballEventService {

    private final FootballEventRepository footballEventRepository;

    public FootballEvent saveFootballEventIfNotExist(FootballEvent footballEvent) {
        return footballEventRepository.findByEventKeyAndEventDate(footballEvent.getEventKey(), footballEvent.getEventDate())
                .map(existingEvent -> {
                    log.warn("Football event with id {} could not be saved in case its already exist for event key {} and date {}", existingEvent.getId(), existingEvent.getEventKey(), existingEvent.getEventDate());
                    return existingEvent;
                })
                .orElseGet(() -> footballEventRepository.save(footballEvent));
    }
}
