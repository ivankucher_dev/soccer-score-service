package com.iomis.service;

import com.iomis.domain.entity.football.event.FootballEvent;
import com.iomis.rest.dto.football.FootballLiveScoreDto;
import com.iomis.service.feign.AllSportsApiFeignClient;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class FootballLiveScoreService {

    private static final String REAL_TIME_SCORE_TYPE = "Livescore";

    @Value("${application.all_sports_api.api_key}")
    private String allSportsApiKey;
    private final AllSportsApiFeignClient allSportsApiFeignClient;

    public FootballLiveScoreDto getFootballLiveScoreDataFromApi() {
        return allSportsApiFeignClient.findTodayFootballMatchesData(REAL_TIME_SCORE_TYPE, allSportsApiKey);
    }

}
